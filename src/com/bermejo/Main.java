package com.bermejo;

import java.util.Scanner;
import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner appScanner = new Scanner(System.in);

        String firstName;
        String lastName;
        double englishGrade;
        double mathGrade;
        double scienceGrade;

        System.out.println("What is your firstname?");
        firstName = appScanner.nextLine().trim();
        System.out.println("What is your lastname?");
        lastName = appScanner.nextLine().trim();
        System.out.println("What is your grade in English?");
        englishGrade = appScanner.nextInt();
        System.out.println("What is your grade in Mathematics?");
        mathGrade = appScanner.nextInt();
        System.out.println("What is your grade in Science?");
        scienceGrade = appScanner.nextInt();

        double aveGrade = ((englishGrade + mathGrade + scienceGrade) / 3);

        DecimalFormat numberFormat = new DecimalFormat("#.##");

        System.out.println("Hi " + firstName + " " + lastName + ", your average grade is " + (numberFormat.format(aveGrade)) + ".");

    }
}
